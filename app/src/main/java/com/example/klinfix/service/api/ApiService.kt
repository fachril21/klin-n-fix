package com.example.klinfix.service.api

import android.content.Context
import android.net.wifi.hotspot2.pps.Credential
import android.provider.SyncStateContract
import com.example.klinfix.model.User
import com.example.klinfix.model.UserResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.nio.charset.StandardCharsets.UTF_8
import java.util.concurrent.TimeUnit

class ApiService {

    private val BASE_URL ="https://api.klinnfix.com/V1/"


    private var apiService : IApiService


    companion object{
        val instance = ApiService()
    }

    init {
        val retrofit: Retrofit = createAdapter()
        apiService = retrofit.create(IApiService::class.java)
    }

//    private fun getClient(): Retrofit{
//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//        val client = OkHttpClient.Builder()
//        client.readTimeout(60, TimeUnit.SECONDS)
//        client.writeTimeout(60, TimeUnit.SECONDS)
//        client.connectTimeout(60, TimeUnit.SECONDS)
//        client.addInterceptor(interceptor)
//        client.addInterceptor(object : Interceptor {
//            override fun intercept(chain: Interceptor.Chain): Response {
//                var request = chain.request()
//                request = request
//                        .newBuilder()
//                        .addHeader("Authorization", "Basic " + "ZmFjaHJpbDIxOkFiY2RlMTIxKg==")
//                        .build()
//
//                return chain.proceed(request)
//            }
//        })
//
//        val retrofit = Retrofit.Builder()
//            .baseUrl(BASE_URL)
//            .client(client.build())
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//
//        return retrofit
//    }

    fun createAdapter(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(
        GsonConverterFactory.create()
    ).build()

    fun requestRegister(user: JsonObject): Call<UserResponse>{
        return apiService.requestRegister(user)
    }

    fun requestLogin(credential: String, username: String, password: String): Call<JsonObject>{
        return apiService.requestLogin(credential, User("", username, password, ""))
    }

    fun requestUser(credential: String): Call<JsonObject>{
        return apiService.requestUser(credential)
    }
}