package com.example.klinfix.service.api

import com.example.klinfix.model.User
import com.example.klinfix.model.UserResponse
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface IApiService {



    @Headers("Content-Type: application/json")
    @POST("register/user")
    fun requestRegister(@Body body: JsonObject) : Call<UserResponse>

    @POST("auth/user")
    fun requestLogin(@Header("Authorization") auth: String, @Body user: User) : Call<JsonObject>

    @POST("user/profile")
    fun requestUser(@Header("Authorization") auth: String) : Call<JsonObject>
}