package com.example.klinfix.model

data class Transaction(
    var id: String,
    var partner: String,
    var status: String,
    var date_service: String
)