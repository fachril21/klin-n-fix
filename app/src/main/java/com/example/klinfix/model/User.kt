package com.example.klinfix.model
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

data class User(
    @SerializedName("name")
    var name: String,

    @SerializedName("username")
    var username: String,

    @SerializedName("password")
    var password: String,

    @SerializedName("email")
    var email: String
//
//    @SerializedName("address")
//    var address: String,
//
//    @SerializedName("status")
//    var status: String,
//
//    @SerializedName("transaction")
//    var transacation: ArrayList<Objects>
)