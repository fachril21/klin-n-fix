package com.example.klinfix.model

data class UserResponse(
    var code: String,
    var data: User,
    var message: String,
    var error: String
)