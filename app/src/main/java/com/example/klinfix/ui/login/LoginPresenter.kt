package com.example.klinfix.ui.login

import android.content.Context
import com.example.klinfix.model.User
import com.example.klinfix.model.UserResponse
import com.example.klinfix.service.api.ApiService
import com.google.gson.JsonObject
import okhttp3.Credentials
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter (val loginView: LoginView.View, val context: Context): LoginView.Presenter{


    override fun requestLogin(username: String, password: String) {

        val credential = Credentials.basic(username, password)

        ApiService.instance.requestLogin(credential, username, password).enqueue( object : Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful){
                    loginView.onFailure(response.body().toString())
                    val sharedPreferences = context.getSharedPreferences("credential", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("credential", credential)
                    editor.apply()
                    loginView.onSuccess()
                } else {
                    loginView.onFailure(response.body().toString())
                }

            }

        })
    }

}