package com.example.klinfix.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.klinfix.R
import com.example.klinfix.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView.View, View.OnClickListener {

    val loginPresenter = LoginPresenter(this, this)

    private lateinit var username: String
    private lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        btn_login.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_login -> loginPresenter.requestLogin(et_username.text.toString(), et_password.text.toString())
        }
    }

    override fun onSuccess() {
//        Toast.makeText(this, "Yeay, login completed", Toast.LENGTH_SHORT).show()
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onFailure(message: String) {
        Log.d("oioioioi", message)
        Toast.makeText(this, "ERROR : "+message, Toast.LENGTH_LONG).show()
    }

}
