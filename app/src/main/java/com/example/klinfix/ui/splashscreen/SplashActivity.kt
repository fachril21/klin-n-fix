package com.example.klinfix.ui.splashscreen

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.klinfix.R
import com.example.klinfix.ui.login.LoginActivity
import com.example.klinfix.ui.main.MainActivity

class SplashActivity : AppCompatActivity(), SplashView.View {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val sharedPreferences = this.getSharedPreferences("credential", Context.MODE_PRIVATE)
        val credential = sharedPreferences.getString("credential", null)

        if (credential != null) {
            toast(credential)
        }


        Handler().postDelayed({

            if (credential != null){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, 4000)

    }


    override fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun toLogin() {
        Toast.makeText(this, "Ke login", Toast.LENGTH_LONG).show()
    }

    override fun toMain() {
        Toast.makeText(this, "Ke Main", Toast.LENGTH_LONG).show()
    }
}
