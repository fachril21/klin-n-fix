package com.example.klinfix.ui.register

import com.example.klinfix.model.User

interface RegisterView {
    interface View{
        fun onSuccess()
        fun onFailure(message: String)
    }
    interface Presenter{
        fun requestRegister(name: String, username: String, password: String, email:String)
    }
}