package com.example.klinfix.ui.register

import android.util.Log
import com.example.klinfix.model.User
import com.example.klinfix.model.UserResponse
import com.example.klinfix.service.api.ApiService
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterPresenter (val registerView: RegisterView.View):
    RegisterView.Presenter {
    override fun requestRegister(name: String, username: String, password: String, email:String) {

        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)
        jsonObject.addProperty("email", email)


        ApiService.instance.requestRegister(jsonObject).enqueue(object : Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.isSuccessful){
                    registerView.onFailure(response.body().toString())
                   registerView.onSuccess()
                } else{
                    registerView.onFailure(response.body().toString())
                }
            }


        })
    }

}