package com.example.klinfix.ui.main.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.klinfix.R
import com.example.klinfix.ui.main.ui.history.fix.FixHistoryFragment
import com.example.klinfix.ui.main.ui.history.klin.KlinHistoryFragment
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_history, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val historyAdapter = HistoryAdapter(fragmentManager)

        historyAdapter.addFragment(KlinHistoryFragment(), resources.getString(R.string.history_klin))
        historyAdapter.addFragment(FixHistoryFragment(), resources.getString(R.string.history_fix))

        view_pager_history.adapter = historyAdapter
        tabs_history.setupWithViewPager(view_pager_history)
        (activity as AppCompatActivity).supportActionBar?.elevation = 0f
    }
}