package com.example.klinfix.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.klinfix.R
import com.example.klinfix.model.User
import com.example.klinfix.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), RegisterView.View, View.OnClickListener {

    val registerPresenter = RegisterPresenter(this)
    private lateinit var user: User
    private lateinit var name: String
    private lateinit var username: String
    private lateinit var password: String
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        supportActionBar?.hide()

        btn_register.setOnClickListener(this)
        btn_signIn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_register -> {
                validate()
                registerPresenter.requestRegister(et_name.text.toString(), et_username.text.toString(), et_password.text.toString(), et_email.text.toString())
            }
            R.id.btn_signIn -> startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onSuccess() {
//        Toast.makeText(this, "Yeay, register completed", Toast.LENGTH_SHORT).show()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onFailure(message: String) {
        Toast.makeText(this, "ERROR : $message", Toast.LENGTH_LONG).show()
    }

    fun validate(){
        if (et_name.text.isEmpty()){
            et_name.error = "Name is empty"
        }
        if (et_username.text.isEmpty()){
            et_username.error = "Username is empty"
        }
        if (et_email.text.isEmpty()){
            et_email.error = "Email is empty"
        }
        if (et_password.text.isEmpty()){
            et_password.error = "Password is empty"
        }

        if(et_name.text.length < 8){
            et_name.error = "8 Character Minimum"
        }
        if(et_username.text.length < 8){
            et_username.error = "8 Character Minimum"
        }
        if(et_password.text.length < 8){
            et_password.error = "8 Character Minimum"
        }

    }
}
