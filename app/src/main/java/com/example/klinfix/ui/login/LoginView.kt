package com.example.klinfix.ui.login


interface LoginView {
    interface View{
        fun onSuccess()
        fun onFailure(message: String)
    }
    interface Presenter{
        fun requestLogin(username: String, password: String)
    }
}