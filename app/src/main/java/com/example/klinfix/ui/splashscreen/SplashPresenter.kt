package com.example.klinfix.ui.splashscreen

import android.content.Context
import com.example.klinfix.service.api.ApiService
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashPresenter (val splashView: SplashView.View, val context: Context): SplashView.Presenter{
    override fun requestUser(credential: String) {
        ApiService.instance.requestUser(credential).enqueue( object : Callback<JsonObject>{
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {

            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful){

                    if (credential != null) {
                        if (credential.isEmpty()){
                            splashView.toast(response.body().toString())
//                            splashView.toLogin()
                        } else{
                            splashView.toast(response.errorBody()?.string().toString())
//                            splashView.toMain()
                        }
                    }
                } else {
                    splashView.toast(response.errorBody()?.string().toString())
                }
            }

        })
    }

}