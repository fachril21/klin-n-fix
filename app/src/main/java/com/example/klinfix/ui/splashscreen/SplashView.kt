package com.example.klinfix.ui.splashscreen

import android.net.wifi.hotspot2.pps.Credential

interface SplashView {
    interface View{
        fun toast(message: String)
        fun toLogin()
        fun toMain()
    }
    interface Presenter{
        fun requestUser(credential: String)
    }
}